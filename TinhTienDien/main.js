var ketQua = function () {
  var tenNguoiDung = document.getElementById("txt-name").value;
  var dienTieuThu = document.getElementById("txt-kwh").value * 1;
  var soTienPhaiDong = null;
  if (0 <= dienTieuThu && dienTieuThu < 50) {
    soTienPhaiDong = dienTieuThu * 500;
  } else if (50 <= dienTieuThu && dienTieuThu < 100) {
    soTienPhaiDong = (dienTieuThu - 50) * 650 + 50 * 500;
  } else if (100 <= dienTieuThu && dienTieuThu < 200) {
    soTienPhaiDong = (dienTieuThu - 100) * 850 + 50 * 650 + 50 * 500;
  } else if (200 <= dienTieuThu && dienTieuThu < 350) {
    soTienPhaiDong =
      (dienTieuThu - 200) * 1100 + 100 * 850 + 50 * 650 + 50 * 500;
  } else if (350 <= dienTieuThu) {
    soTienPhaiDong =
      (dienTieuThu - 350) * 1300 + 150 * 1100 + 100 * 850 + 50 * 650 + 50 * 500;
  }
  document.getElementById(
    "result"
  ).innerHTML = `Chào ${tenNguoiDung}! <br/> Số tiền cần đóng là: ${soTienPhaiDong.toLocaleString()} vnđ`;
};
