var diemKhuVuc = function (diemKV) {
  switch (diemKV) {
    case "A": {
      return 2;
    }
    case "B": {
      return 1;
    }
    case "C": {
      return 0.5;
    }
    default: {
      return 0;
    }
  }
};
var diemDoiTuong = function (diemDT) {
  switch (diemDT) {
    case "1": {
      return 2.5;
    }
    case "2": {
      return 1.5;
    }
    case "3": {
      return 1;
    }
    default: {
      return 0;
    }
  }
};

// var tong = diemDoiTuong() + diemKhuVuc();
// console.log(tong);

var ketQua = function () {
  var diemChuan = document.getElementById("txt-diemChuan").value * 1;
  var diem1 = document.getElementById("txt-diem1").value * 1;
  var diem2 = document.getElementById("txt-diem2").value * 1;
  var diem3 = document.getElementById("txt-diem3").value * 1;
  var diemKV = document.getElementById("txt-KhuVuc").value;
  var diemDT = document.getElementById("txt-DoiTuong").value;
  var xepLoai = null;
  var tongDiem =
    diem1 + diem2 + diem3 + diemDoiTuong(diemDT) + diemKhuVuc(diemKV);

  if (diem1 == 0 || diem2 == 0 || diem3 == 0) {
    xepLoai = "Bạn đã thi trượt do có môn điểm 0";
  } else if (tongDiem <= diemChuan) {
    xepLoai = "Bạn đã thi trượt do điểm không đạt yêu cầu";
  } else {
    xepLoai = "Chúc mừng! bạn đã trúng tuyển";
  }

  console.log(diemDoiTuong());
  document.getElementById(
    "result"
  ).innerHTML = `${xepLoai} - Điểm của bạn là: ${tongDiem}`;
};
